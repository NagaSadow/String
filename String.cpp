#define _CRT_SECURE_NO_WARNINGS
// zeby nie pojawialy sie ostrzezenia o malo bezpiecznych funkcjach

#include <iostream> // potrzebne do wypisywania i pobierania informacji
#include <cstdlib>  // potrzebne do funkcji system

using namespace std;   // *this ---> self referential pointer (stały wskaźnik)

class String {
	char *s;
	int len;
	static int Stan;
public:
	String(void);
	String(int n);
	String(const char *p);
	// konstruktor ktory przyjmuje stala referencje na obiekt typu String
	String(const String& str); // konstruktor kopiujacy
	~String() { delete s; } // destruktor


	int lenght() const {
		return(len);
	}


	String& operator =(const String& str);// operator podstawiania
	void print() const {
		cout << s << "\nLenght: " << len << "\n";
	}

	void operator !() const { print(); }	// print


	operator char*() const { return(s); }	// operator konwersji
	static void switch_stan(void);
	String& operator ++(void);	// Zamienia małe litery na duże i zwraca nowy string
	String operator ++(int);	// Zamienia małe litery na duże i zwraca stary string
	String& operator --(void);	// Zamienia duże litery na małe i zwraca nowy string
	String operator --(int);	// Zamienia duże litery na małe i zwraca stary string


	int operator-(const String& str); //zwraca bezwględną różnicę liczby znaków dwóch stringów
	int operator*(const String& str); // zwraca iloczyn liczby znaków dwóch stringów
	bool operator>(const String& str); //sprawdza czy jeden string jest dłuższy od drugiego
	bool operator<(const String& str); //sprawdza czy jeden string jest krótszy od drugiego
	bool operator==(const String& str); // sprawdza czy dwa stringi są równej długości
	String& operator-(); // zwraca łańcuch pisany od tyłu
	String& operator<<(int n); // usuwa n początkowych znaków i dopisuje n razy "x" na końcu
	String& operator>>(int n); // usuwa n końcowych znaków i dopisuje n razy "x" na początku
	// friend int operator*(const String& l, const String& p);
	friend String operator +(const String str1, const String str2);


};

String::String(void)
{
	s = new char[81];
	s[0] = 0;				// przerwanie łańcucha
	len = 80;
	if (Stan)
	{
		cout << "\nPracuje konstruktor domniemany:\n";
		print();
	}
}

String::String(int n)
{
	s = new char[n + 1];
	s[0] = 0;				// przerwanie łańcucha
	len = n;
	if (Stan)
	{
		cout << "\nPracuje konstruktor z jednym argumentem:\n";
		print();
	}
}

String::String(const char *p)
{
	len = (int)strlen(p);	// rzutowanie
	s = new char[len + 1]; //rezerwujemy miejsce na tekst
	strcpy(s, p); // koiujemy z p do pamieci pod adresem s
	if (Stan)
	{
		cout << "\nPracuje operator konwersji:\n";
		print();
	}
}

String::String(const String& str)
{

	s = 0;
    // zajrzyj pod adres aktualnego obiektu i wstaw wartosc z str
	(*this) = str;
	if (Stan)
	{
		cout << "\nPracuje konstruktor kopiujacy:\n";
		print();
	}
}

// String x = a;
// x  = b = a;
String& String::operator =(const String &str)
{
	len = str.len;
	if (s) delete s;
	s = new char[len + 1];
	strcpy(s, str.s);
	if (Stan)
	{
		cout << "\nPracuje przeciazony operator podstawiania:\n";
		print();
	}
	return *this;
}


int String::Stan = 0;




void String::switch_stan(void)
{
	cout << "\nStan = ";
	cout << (Stan ? "gadamy." : "siedzimy cicho.");
	cout << "    Zmiana? (t/n) ";
	int p = getchar();
	if (p == 't' || p == 'T')
		Stan = Stan ^ 1;
	cout << endl;
}

// ++a;

String& String::operator ++(void)	// Zamienia małe litery na duże i zwraca nowy string
{
	char mm = 'A' - 'a';
	for (int i = 0; i<len; i++)
		if (s[i] >= 'a' && s[i] <= 'z')
			s[i] += mm;
	return (*this);
}

//a++
String String::operator ++(int)	// Zamienia małe litery na duże i zwraca stary string
{
	String temp(*this);
	char mm = 'A' - 'a';
	for (int i = 0; i<len; i++)
		if (s[i] >= 'a' && s[i] <= 'z')
			s[i] += mm;
	return temp;
}

String& String::operator --(void)	// Zamienia duże litery na małe i zwraca nowy string
{
	char mm = 'A' - 'a';
	for (int i = 0; i<len; i++)
		if (s[i] >= 'A' && s[i] <= 'Z')
			s[i] -= mm;
	return (*this);
}

String String::operator --(int)	// Zamienia duże litery na małe i zwraca stary string
{
	String temp(*this);
	char mm = 'A' - 'a';
	for (int i = 0; i<len; i++)
		if (s[i] >= 'A' && s[i] <= 'Z')
			s[i] -= mm;
	return temp;
}
// String x("abc"); len = 3;
// String y("abcdef"); len = 6;
// x - y;
int String::operator-(const String & str)
{
	int r = len - str.len;        // obiekt.składnik
	if (r < 0) {
		r = -r;
	}
	return r;
}

int String::operator*(const String & str)
{
	return len * str.len;
}

bool String::operator>(const String & str)
{
	return len > str.len ;
}

bool String::operator<(const String & str)
{
	return len < str.len;
}

bool String::operator==(const String & str)
{
	return len == str.len;
}

String & String::operator-()
{
	// fed/cba
	//

	char* n = new char[len + 1];
	n[len] = 0; // koniec na koncu tablicy

	int j = len - 1;
	for (int i = 0; i < len; i++) {
		n[i] = s[j];
		j--;
	}

	delete s;
	s = n;

	return *this;
}

String & String::operator<<(int n)
{
	// abc << 2 cxx

	if ((n>len) || (n<0)) {
		return *this;
	}

	char* nstr = new char[len + 1]; //rezerwujemy miejsce na tekst
	nstr[len] = 0;
	// kopiujemy znaki, ktore zostaja na poczatek
	int j = 0;
	for (int i = n; i < len; i++) {
		nstr[j] = s[i];
		j++;
	}

	for (int i = len - n; i < len; i++) {
		nstr[i] = 'x';
	}

	delete s;
	s = nstr;

	return *this;
}



String & String::operator>>(int n)
{
	// abc << 2 cxx

	if ((n>len) || (n<0)) {
		return *this;
	}

	char* nstr = new char[len + 1]; //rezerwujemy miejsce na tekst
	nstr[len] = 0;
	// kopiujemy znaki, ktore zostaja na poczatek
	int j = 0;
	for (int i = 0; i < len - n; i++) {
		nstr[j+n] = s[i];
		j++;
	}

	for (int i = 0; i < n; i++) {
		nstr[i] = 'x';
	}

	delete s;
	s = nstr;

	return *this;
}

String operator +(const String str1, const String str2)
{
	String temp(str1.len + str2.len); // rezerwuje puste miejsce na sume znaków
	strcpy(temp.s, str1.s); // kopiuje peirwsza część
	strcat(temp.s, str2.s); // dokleja na koncu str2
	return temp; // zwrocenie nowo powstałego tekstu
}
int main(void)
{
	String str_a("ala ma kota");
	String str_b("abcdef");


	cout << str_a - str_b << endl;
	cout << str_a * str_b << endl;
	cout << (str_a > str_b) << endl;
	cout << (str_a < str_b) << endl;
	cout << (str_a == str_b) << endl;
	cout << (str_a >> 2) << endl;
	cout << (str_b << 3) << endl;
	cout << (-str_a) << endl;
	cout << (-str_b) << endl;




	String::switch_stan();
	const char *str1 = "Typem, z ktorego dokonuje sie przeksztalcenia ";
	const char *str2 = "nie musi byc typ wbudowany.";

	String a(str1), b(str2);

	String c = "Symfonia C++";
	// String g(a), tworzymy g na podstawie a
	// String x = a;
	String d = String("Jerzy Grebosz");
	cout << endl << " ** Wyniki ** " << endl << endl;
	cout << a + b;
	cout << endl << (a + str2);
	cout << endl << (str1 + b);
	cout << "\n" + c + "\n" + d;
	// Testowanie operatorów inkrementacji i dekrementacji
	cout << endl << endl;
	cout << endl << a;
	cout << endl << ++a;
	cout << endl << a;
	cout << endl << endl;
	cout << endl << b;
	cout << endl << b++;
	cout << endl << b;
	cout << endl << endl;
	cout << endl << c;
	cout << endl << --c;
	cout << endl << c;
	cout << endl << endl;
	cout << endl << d;
	cout << endl << d--;
	cout << endl << d;


	system("pause");
	return 0;
}
